import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {CoreModule} from "./core/core.module";
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {ProductsService} from "./features/products/services/products.service";
import {LogService, LOGTYPE} from "./core/services/log.service";
import {FakeLogService} from "./core/services/fake-log.service";
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CoreModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: LOGTYPE, useValue: 'VERBOSE'
    },
    {
      provide: LogService, useFactory: (http: HttpClient, logType: string) => {
        return environment.production ? new LogService(http, logType) : new FakeLogService(http, logType)
      },
      deps: [HttpClient, LOGTYPE]
    }
  ],
  exports: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

}

