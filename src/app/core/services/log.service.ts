import {Inject, Injectable, InjectionToken} from '@angular/core';
import {HttpClient} from "@angular/common/http";

export const LOGTYPE = new InjectionToken('LOG TYPE')


@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(http: HttpClient, @Inject('LOGTYPE') logType: string) {
    console.log('log service', logType)
  }
}
