import {Inject, Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class FakeLogService {


  constructor(http: HttpClient, @Inject('LOGTYPE') logType: string) {
    console.log('fake log service', logType)
  }
}
