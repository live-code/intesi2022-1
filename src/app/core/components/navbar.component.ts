import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'int-navbar',
  template: `
    <div class="flex gap-2 justify-center flex-col sm:flex-row m-3">
      <button class="my-btn" routerLink="login">login</button>
      <button class="my-btn" routerLink="catalog">catalog</button>
      <button class="my-btn" routerLink="uikit">uikit</button>
      <button class="my-btn" routerLink="products">products</button>
      <button class="my-btn" routerLink="contacts">contacts</button>
      <button class="my-btn" routerLink="users">users</button>
    </div>
  `,
  styles: [`
    .my-btn {
      @apply bg-slate-400 rounded-xl p-2 hover:bg-slate-200 transition
    }
  `]
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
