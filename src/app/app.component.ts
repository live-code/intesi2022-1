import { Component } from '@angular/core';

@Component({
  selector: 'int-root',
  template: `
    <int-navbar></int-navbar>
    <hr>
    <div class="m-4">
        <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  title = 'intesi2022-1';
}
