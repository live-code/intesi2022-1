import {
  AfterContentInit,
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren, Input,
  OnInit,
  QueryList,
  ViewChildren
} from '@angular/core';
import {AccordionGroupComponent} from "./accordion-group.component";

@Component({
  selector: 'int-accordion',
  template: `
    <div class="border p-2 bg-slate-300">
      <div *ngIf="something"></div>
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(AccordionGroupComponent) groups!: QueryList<AccordionGroupComponent>
  @Input() multipleOpen = false;
  @Input() something: string = '';

  ngAfterContentInit() {
    const items = this.groups.toArray()
    items.forEach(item => {
      item.open = false;
      item.openChange.subscribe(() => {
        if (!this.multipleOpen) {
          this.closeAll();
          item.open = true;
        } else {
          item.open = !item.open;
        }
      })
    })
    items[0].open = true;
  }

  closeAll() {
    this.groups.toArray()
      .forEach(item => {
        item.open = false;
      })
  }
}
