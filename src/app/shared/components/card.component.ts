import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'int-card',
  template: `
    <div [ngClass]="{'mb-2': marginBottom}">
      <div
        class="flex justify-between items-center bg-slate-900 text-white p-2"
        [ngClass]="headerCls"
        (click)="open = !open"
      >
        {{title}}

        <i [class]="icon" (click)="iconHandler($event)"></i>
      </div>
      <div
        class="p-2 border border-slate-900"
        *ngIf="open"
      >
        <ng-content></ng-content>
      </div>
    </div>

  `,
})
export class CardComponent {
  @Input() title: string | undefined;
  @Input() icon: string | undefined;
  @Input() marginBottom: boolean = false;
  @Input() headerCls: string = '';
  @Output() iconClick = new EventEmitter();

  open = true;

  iconHandler(event: MouseEvent) {
    event.stopPropagation();
    this.iconClick.emit()
  }
}
