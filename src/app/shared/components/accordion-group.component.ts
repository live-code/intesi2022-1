import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AccordionComponent} from "./accordion.component";

@Component({
  selector: 'int-accordion-group',
  template: `


    <div>
      <div
        class="flex justify-between items-center bg-slate-900 text-white p-2"
        (click)="toggle()"
      >
        {{title}} - {{open}}

      </div>
      <div
        class="p-2 border border-slate-900"
        *ngIf="open"
      >
        <ng-content></ng-content>
      </div>
    </div>

  `,
})
export class AccordionGroupComponent {
  @Input() title: string | undefined;
  @Input() open = true;
  @Output() openChange = new EventEmitter()

  constructor(private accordion: AccordionComponent) {
  }

  ngOnInit() {
    console.log(this.accordion.something)
    this.accordion.something = '345'
  }

  toggle() {
    // this.open = !this.open
    this.openChange.emit(this.open)
  }
}
