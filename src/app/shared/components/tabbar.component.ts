import {ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'int-tabbar',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `

    <div class="flex gap-2">
      <div
        *ngFor="let item of items"
        [ngClass]="{'active': item.id === selectedItem?.id}"
        (click)="tabClick.emit(item)"
      >
       {{getTitle(item)}}
      </div>
    </div>
  `,
  styles: [`
    .active {
      @apply bg-slate-200
    }
  `]
})
export class TabbarComponent <T extends { id: number }>  {
// export class TabbarComponent <T extends { id: number }, K extends keyof T>  {
  @Input() items: T[] = [];
  @Input() selectedItem: T | null = null;
  @Input() labelField: string = 'label'
  @Output() tabClick = new EventEmitter<T>();



  getTitle<T>(item: { [key: string]: any}) {
    return item[this.labelField]
  }

}
