import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CardComponent} from "./components/card.component";
import { TabbarComponent } from './components/tabbar.component';
import {CardModule} from "./components/card.module";
import {TabbarModule} from "./components/tabbar.module";
import {AccordionGroupComponent} from "./components/accordion-group.component";
import {AccordionComponent} from "./components/accordion.component";



@NgModule({
  declarations: [
    AccordionGroupComponent,
    AccordionComponent
  ],
  imports: [
    CommonModule,
    CardModule,
    TabbarModule
  ],
  exports: [
    CardModule,
    TabbarModule,
    AccordionComponent,
    AccordionGroupComponent
  ]
})
export class SharedModule { }
