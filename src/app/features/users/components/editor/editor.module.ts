import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EditorComponent} from "./editor.component";
import {TilesComponent} from "./components/tiles.component";
import {TextComponent} from "./components/text.component";
import {ShapesComponent} from "./components/shapes.component";



@NgModule({
  declarations: [
    EditorComponent,
    TilesComponent,
    TextComponent,
    ShapesComponent
  ],
  exports: [
    EditorComponent
  ],
  imports: [
    CommonModule,
  ]
})
export class EditorModule { }
