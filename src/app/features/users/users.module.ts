import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { GroupsComponent } from './components/groups/groups.component';
import { ListComponent } from './components/list/list.component';
import { RulesComponent } from './components/rules/rules.component';
import {EditorModule} from "./components/editor/editor.module";
import {SharedModule} from "../../shared/shared.module";
import {CardModule} from "../../shared/components/card.module";


const routes: Routes = [
  { path: '', component: UsersComponent }
];

@NgModule({
  declarations: [
    UsersComponent,
    GroupsComponent,
    ListComponent,
    RulesComponent,
  ],
  exports: [
    ListComponent
  ],
  imports: [
    CardModule,
    CommonModule,
    EditorModule,
    RouterModule.forChild(routes)
  ]
})
export class UsersModule { }
