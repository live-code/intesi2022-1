import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'int-users',
  template: `
        <h1>Users</h1>
        <int-card></int-card>
        <int-editor></int-editor>
        <int-groups></int-groups>
        <int-list></int-list>
        <int-rules></int-rules>
  `,
  styles: [
  ]
})
export class UsersComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
