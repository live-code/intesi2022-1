import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {LoginComponent} from "./login.component";
import { SigninComponent } from './pages/signin/signin.component';
import { LostpassComponent } from './pages/lostpass/lostpass.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import {CardComponent} from "../../shared/components/card.component";
import {SharedModule} from "../../shared/shared.module";
import {CardModule} from "../../shared/components/card.module";

@NgModule({
  declarations: [
    LoginComponent,
      SigninComponent,
      LostpassComponent,
      RegistrationComponent,
  ],
  imports: [
    CommonModule,
    CardModule,
    RouterModule.forChild([
      {
        path: '',
        component: LoginComponent,
        children: [
          { path: 'signin', component: SigninComponent},
          { path: 'lostpassword', component: LostpassComponent},
          { path: 'registration', component: RegistrationComponent},
          { path: '', redirectTo: 'signin'}
        ]
      },
    ])
  ]
})
export class LoginModule { }
