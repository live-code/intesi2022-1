import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'int-login',
  template: `

    <h1 *ngIf="true">Login</h1>
    <int-card></int-card>
    <router-outlet></router-outlet>

    <hr>
    <button routerLink="registration">Registration</button>
    <button routerLink="../login/lostpassword">Lost pass</button>
    <button routerLink="/login/signin">SignIn</button>
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
