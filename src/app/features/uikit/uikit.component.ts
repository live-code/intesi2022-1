import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {LogService} from "../../core/services/log.service";
import {debounceTime, distinctUntilChanged, fromEvent, map} from "rxjs";

interface City {
  id: number;
  name: string
}
interface Country {
  id: number;
  label: string;
  desc: string;
  cities: City[]
}

@Component({
  selector: 'int-uikit',
  template: `
    <h3 class="text-2xl">Card Example</h3>

    <int-card
      [marginBottom]="false"
      headerCls="bg-pink-500"
      title="1+1" icon="fa fa-google"
      (iconClick)="openUrl('http://www.google.com')"
    >
      copyright 2022 bla bla
    </int-card>

    <int-card
      icon="fa fa-eye"
      (iconClick)="visible = !visible"
      [title]="'pluto ewfihew fhew hf wehfew hfuw ehf wehf ehw hfweh hfewh f9weh few'"
    >
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur atque cupiditate, deleniti dolore eligendi, expedita harum maiores, minima molestiae nam numquam odit officia tenetur velit voluptas. Distinctio ducimus impedit suscipit?
    </int-card>

    <div *ngIf="visible">
      something to see
    </div>


    <div #parentBtn>
      <button #btn>DO SOMETHING</button>
    </div>
    <input type="text" #inputRef class="border">

    <int-tabbar
      [items]="countries"
      [selectedItem]="activeCountry"
      (tabClick)="selectCountry($event)"
    ></int-tabbar>

    <int-tabbar
      *ngIf="activeCountry"
      labelField="name"
      [items]="activeCountry.cities"
      [selectedItem]="activeCity"
      (tabClick)="selectCity($event)"
    ></int-tabbar>

    <img
      width="100%"
      [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center='  + activeCity?.name + '&size=600,400'" alt="">

  `,
})
export class UikitComponent implements AfterViewInit {
  countries: Country[] = [];
  activeCountry: Country | null = null;
  activeCity: City | null = null;

  selectCountry(country: Country) {
    this.activeCountry = country;
    this.activeCity = this.activeCountry.cities[0]
  }

  selectCity(city: City) {
    this.activeCity = city;
  }



  @ViewChild('parentBtn') parentButton!: ElementRef<HTMLButtonElement>;
  @ViewChild('btn') button!: ElementRef<HTMLButtonElement>;
  @ViewChild('inputRef') inputRef!: ElementRef<HTMLInputElement>
  visible = false;

  constructor(private log: LogService) {
    // http....
    setTimeout(() => {
      this.countries = [
        {
          id: 2,
          label: 'germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, name: 'Berlin' },
            { id: 2, name: 'Monaco' }
          ]
        },
        {
          id: 1, label: 'italy', desc: 'bla bla 1',   cities: [
            { id: 11, name: 'Rome' },
            { id: 22, name: 'Milan' },
            { id: 33, name: 'Palermo' },
          ]
        },
        { id: 3, label: 'spain', desc: 'bla bla 3', cities: [
            {id: 41, name: 'Madrid'}
          ]},
      ];
      this.selectCountry(this.countries[0])
    }, 1000)
  }

  ngAfterViewInit(): void {
    fromEvent(this.inputRef.nativeElement, 'input')
      .pipe(
        map(ev => (ev.target as HTMLInputElement)?.value),
        debounceTime(1000),
        distinctUntilChanged()
      )
      .subscribe(text => {
        console.log('do http', text)
      });

    this.inputRef.nativeElement.focus()

    this.button.nativeElement.addEventListener('click', () => {
      console.log('do something child')
    }, true)

    this.parentButton.nativeElement.addEventListener('click', () => {
      console.log('do something parent')
    }, true)
  }

  openUrl(url: string) {
    window.open(url)
  }

  doSOmethingParent() {
    console.log('doSOmethingParent')
  }

}
