import {NgModule} from "@angular/core";
import {CatalogComponent} from "./catalog.component";
import { OffersComponent } from './components/offers.component';
import { ImagePreviewComponent } from './components/image-preview.component';
import { ProductsListComponent } from './components/products-list.component';
import {RouterModule} from "@angular/router";
import {SharedModule} from "../../shared/shared.module";
import {LogService} from "../../core/services/log.service";
import {FakeLogService} from "../../core/services/fake-log.service";
import {FormsModule} from "@angular/forms";
import {CommonModule} from "@angular/common";

@NgModule({
  declarations: [
    CatalogComponent,
    OffersComponent,
    ImagePreviewComponent,
    ProductsListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild([
      {path: '', component: CatalogComponent}
    ]),
    FormsModule
  ],
  providers: [

/*
    { provide: LogService, useClass: FakeLogService}
*/
  ]
})
export class CatalogModule {}

