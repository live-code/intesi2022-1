import {AfterViewInit, ChangeDetectorRef, Component, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {LogService} from "../../core/services/log.service";
import {AccordionComponent} from "../../shared/components/accordion.component";
import {AccordionGroupComponent} from "../../shared/components/accordion-group.component";

@Component({
  selector: 'int-catalog',
  template: `

    <form #f="ngForm">
      {{usernameRef.errors | json}}
      <input
        #usernameRef="ngModel"
        type="text" ngModel name="username" placeholder="username" required minlength="3">
    </form>

    <int-accordion [multipleOpen]="false"  something="123">
      <int-accordion-group title="1">
        content
      </int-accordion-group>
      <int-accordion-group title="2">
        content
      </int-accordion-group>
      <int-accordion-group title="3">
        content
      </int-accordion-group>
    </int-accordion>


  `,
})
export class CatalogComponent  {

}
