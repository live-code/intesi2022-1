import {Component, Input, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ProductsService} from "../services/products.service";

@Component({
  selector: 'int-product-list',
  template: `
      <h1>{{cat}}</h1>
      <li
        (click)="open = true"
        *ngFor="let item of itemsService.items">
        {{item[labelField]}}
        <button (click)="itemsService.delete(item.id)">delete</button>
      </li>

      <div *ngIf="open">modal</div>
  `,
  providers: [
    ProductsService
  ]
})
export class ListComponent {
  @Input() cat: string = '';
  @Input() labelField = 'name'
  open = false;

  constructor(
    public itemsService: ProductsService,
  ) {}

  ngOnInit() {
    this.itemsService.init(this.cat)
    this.itemsService.getAll()
  }

}
