import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'int-products',
  template: `
    <h1>products</h1>
    <int-product-list cat="users"></int-product-list>
    <int-product-list cat="posts"
                      labelField="title"></int-product-list>
  `,
})
export class ProductsComponent  {

}



