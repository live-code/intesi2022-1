import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProductsComponent} from "./products.component";
import { ListComponent } from './components/list.component';
import {RouterModule} from "@angular/router";
import {ProductsService} from "./services/products.service";

@NgModule({
  declarations: [
    ProductsComponent,
    ListComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: ProductsComponent}
    ])
  ],
/*
  providers: [
    ProductsService
  ]*/

})
export class ProductsModule { }
