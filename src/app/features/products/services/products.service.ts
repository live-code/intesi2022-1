import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable()
export class ProductsService {
  items: any[] = [];
  cat: string = '';

  constructor(private http: HttpClient) {
    console.log('ciao')
  }

  init(cat: string) {
    this.cat = cat;
  }

  getAll() {
    this.http.get<any[]>('https://jsonplaceholder.typicode.com/' + this.cat)
      .subscribe(res => {
        this.items = res;
      })
  }

  delete(id: string) {
    this.http.delete(`https://jsonplaceholder.typicode.com/${this.cat}/${id}`)
      .subscribe(res => {
        const index = this.items.findIndex(item => item.id === id)
        this.items.splice(index, 1)
      })
  }


}
