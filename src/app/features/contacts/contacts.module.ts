import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from "@angular/router";
import {ContactsComponent} from "./contacts.component";
import {TabbarModule} from "../../shared/components/tabbar.module";



@NgModule({
  declarations: [
    ContactsComponent
  ],
  imports: [
    CommonModule,
    TabbarModule,
    RouterModule.forChild([
      { path: '', component: ContactsComponent}
    ])
  ],

})
export class ContactsModule { }
