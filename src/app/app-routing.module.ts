import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

const routes = [
  { path: 'products',  loadChildren: () => import('./features/products/products.module').then(m => m.ProductsModule)},
  {path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule)},
  {
    path: 'contacts', loadChildren: async function () {
      const res = await import('./features/contacts/contacts.module')
      return res.ContactsModule
    }
  },
  {path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule)},
  {path: 'users', loadChildren: () => import('./features/users/users.module').then(m => m.UsersModule)},
  { path: 'uikit', loadChildren: () => import('./features/uikit/uikit.module').then(m => m.UikitModule) },
  {path: '**', redirectTo: 'login'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
